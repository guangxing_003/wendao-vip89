# Author: Lovy
# File : ui_test
# Time : 2023-02-17 10:34

from PyGameAutoAndroid import *

# 创建ui对象
ui = xp.ui()

if ui:
    account = ui.stringValue("login_account_input")
    print("账户名:{}".format(account))
    password = ui.stringValue("login_password_input")
    print("密码:{}".format(password))
    ret = ui.boolValue("language_Python")
    print(ret)



# Author: Lovy
# File : main
# Time : 2023-02-26 15:31
from libs import *
import time


# 只做 除暴 修行
def expr_task():
    Pet.show_life(select=True)
    # 默认除暴
    xp.console("执行除暴。。。")
    if get_all_task_loc(task_names="除暴", old_task_names="除余暴爆"):
        goto_chubao()

    xp.console("执行修行。。。")
    if get_all_task_loc(task_names="修行", old_task_names="修行"):
        goto_xiuxing()

    xp.console("执行副本。。。")
    if get_all_task_loc(task_names="副本", old_task_names="副畐本"):
        goto_fuben()

    # 检测是否处于组队还是任务
    switch_zhudui_renwu(task=False)
    click(mainUI["main_team_loc"]["队伍离队"], text="点击 离队")
    click(main_task["确认"], text="点击 确认")


def branch_main(vip=False):
    # 循环找色 赤灵尊神
    res = loop_until(main_task["find_color"], "人物对话叉号", times=2)
    if res:
        click(main_task["对话"]["first_button"], text="领取副本奖励")
        click(main_task["对话"]["second_button"], text="领取道行奖励")
    time.sleep(4)
    main_patrol_setting(defence=True)
    Man.show_attr()

    not_finished_task_ocr()
    xp.console("执行助人为乐。。。")
    if get_all_task_loc(task_names="助人为乐", old_task_names="助人为乐"):
        goto_zhuren(vip=vip)
    xp.console("执行帮派任务。。。")
    if get_all_task_loc(task_names="帮派任务"):
        goto_bangpai(vip=vip)
    xp.console("执行帮派日常任务。。。")
    if get_all_task_loc(task_names="帮派日常", old_task_names="日常"):
        goto_bangpairichang(vip=vip)
    xp.console("执行竞技场。。。")
    if get_all_task_loc(task_names="竞技场", old_task_names="竞技场"):
        goto_jingjichang()
    xp.console("执行通天塔")
    if get_all_task_loc(task_names="通天塔", old_task_names="通天"):
        goto_tongtian(vip=vip)
    xp.console("师门任务。。。")
    if get_all_task_loc(task_names="师门", old_task_names="师门"):
        goto_shimen(vip=vip)
    xp.console("执行引魂入殿任务。。。")
    if get_all_task_loc(task_names="引魂入殿", old_task_names="引魂"):
        goto_yinhunrudian()
    xp.console("魔龙之祸。。。")
    if get_all_task_loc(task_names="魔龙之祸", old_task_names="魔龙"):
        goto_molong(vip=vip)
    xp.console("修法任务。。。")
    if get_all_task_loc(task_names="修法", old_task_names="法"):
        goto_xiufa(vip=vip)

    # 增加寿命 强化 开启天书
    Pet.show_life(increase_strengthen=True, increase_celestial=True, select=True)
    xp.console("领取活跃奖励")
    get_active_award()
    xp.console("领取邮件")
    get_emial_award()
    xp.console("点击荣耀 膜拜")
    get_honour_award()
    xp.console("领取洛书")
    get_luoshu_award()
    xp.console("领取元宝")
    if vip:
        get_sliver_ingot()
    xp.console("周一帮贡")
    get_bangpai_welfare()
    xp.console("领取福利")
    get_welfare()


##########################################################
if __name__ == '__main__':
    time.sleep(5)
    # expr_task()
    vip = False
    time.sleep(5)
    branch_main(vip=vip)





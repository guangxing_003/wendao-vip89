# Author: Lovy
# File : 洛书
# Time : 2023-02-26 16:30
from common.generalFunction import *


# 洛书 点击 领取
def get_luoshu_award():
    # 主界面 循环找图 展开
    res = loop_Image_until(mainUI["find_image"]["展开"], times=5)
    if res:
        click([res.x, res.y, res.x + 40, res.y + 2], text="点击 展开")
        res1 = loop_Image_until(mainUI["find_image"]["洛书"], times=5)
        if res1:
            click([res1.x, res1.y, res1.x + 40, res1.y + 2], text="点击 洛书")
            res2 = loop_Image_until(mainUI["find_image"]["洛书领取"], times=5)
            if res2:
                click([res2.x + 25, res2.y + 12, res2.x + 85, res2.y + 40], text="点击 洛书领取")
            click([1130, 91, 1157, 116], text="点击 退出")
    time.sleep(3)

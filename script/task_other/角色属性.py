# Author: Lovy
# File : 角色属性
# Time : 2023-02-26 16:09
from common.generalFunction import *
import traceback


# 宠物补充
class Pet:
    right_loc = main_pet["right_loc"]
    cancel = main_pet["叉号"]
    property = main_pet["属性"]
    skill = main_pet["技能"]

    @classmethod
    def show_life(cls, increase_strengthen=False, increase_celestial=False, select=False):
        """点击属性 点击加号 + 点击使用"""
        click(mainUI["main_pet_loc"], text="点击 宠物位置")
        click(cls.right_loc["属性"], text="点击 属性")
        life_nums = ocr(cls.property["寿命"])
        # 叉掉
        click(cls.cancel, text="点击 叉号")
        xp.console("宠物寿命:{}".format(life_nums))
        log.info("宠物寿命:{}".format(life_nums))
        try:
            if int(life_nums) < 9000:
                cls.add_life()
        except Exception as error:
            log.error("宠物寿命识别有问题;{}".format(traceback.format_exc()))
            print("宠物寿命识别有问题;{}".format(traceback.format_exc()))
            return
        # todo 完成增加寿命流程 强化丹强化 天书灵气补充 开关
        cls.strengthen(increase_strengthen)
        cls.celestial_script(increase_celestial)
        # False 表示取消 天书使用
        cls.switch_celestial(select)
        time.sleep(5)

    @classmethod
    def add_life(cls):
        click(mainUI["main_pet_loc"], text="点击 宠物位置")
        click(cls.right_loc["属性"], text="点击 属性")
        click(cls.right_loc["阳间"], text="点击 阳间")
        click(cls.property["寿命加号"], text="点击 寿命加号")
        click(cls.property["神兽丹加号"], text="点击 神兽丹加号")
        click(cls.property["使用"], text="点击 使用")
        click(cls.property["确认"], text="点击 确认")
        click(cls.cancel, text="点击 叉号")

    @classmethod
    def strengthen(cls, increase_strengthen=False):
        """点击 强化丹"""
        if increase_strengthen:
            click(mainUI["main_pet_loc"], text="点击 宠物位置")
            click(cls.right_loc["属性"], text="点击 属性")
            click(cls.right_loc["阳间"], text="点击 阳间")
            click(cls.property["成长"], text="点击 成长")
            click(cls.property["column"]["强化栏"], text="点击 强化栏")
            strength_nums = ocr(cls.property["强化丹数量"])
            # print(nums)
            try:
                nums = strength_nums.split("/")[0]
                num = int(nums)
                while num > 1:
                    click(cls.property["强化"], text="点击 强化")
                    click(cls.property["确认"], text="点击 确认")
                    num -= 1
            except Exception as error:
                log.error("强化丹识别有问题;{}".format(traceback.format_exc()))
                print("强化丹识别有问题;{}".format(traceback.format_exc()))
            click(cls.property["叉号"], text="点击 叉号 宠物强化")
            click(cls.cancel, text="点击 叉号")

    @classmethod
    def celestial_script(cls, increase=True):
        """点击 天书 添加灵气"""
        """提升数字识别准确度 向上5个像素点 向下7个像素点  向右15个像素点"""
        if increase:
            click(mainUI["main_pet_loc"], text="点击 宠物位置")
            click(cls.right_loc["阳间"], text="点击 阳间")
            click(cls.right_loc["技能"], text="点击 技能")
            click(cls.skill["row"]["天书"], text="点击 天书")
            names = []
            for i in cls.skill["天书"]["名称"]:
                names.append(ocr(i))
            print(names)

            for j in cls.skill["天书"]["灵气"]:
                ret = ocr(j)
                print("天书灵气:{}".format(ret))
                log.info("天书灵气:{}".format(ret))
                """灵气小于5000进行补充"""
                try:
                    if int(ret) < 5000:  # 可能int装换数字会有问题ret中有字母
                        click(j, text="点击 灵气")
                        click(cls.skill["天书"]["补充灵气"], text="点击 补充灵气")
                        click(cls.skill["天书"]["元宝补充"], text="点击 元宝补充")
                        # 先补充一百元宝
                        click(cls.skill["元宝补充"]["一百元宝"], text="点击 一百元宝")
                        click(cls.skill["元宝补充"]["确认"], text="点击 确认")
                        # 叉号 元宝补充
                        click(cls.skill["元宝补充"]["叉号"], text="点击 元宝补充叉号")
                        # 叉号 提交天书
                        click(cls.skill["天书"]["叉号"], text="点击 提交天书叉号")
                except Exception as error:
                    log.error("识别有问题;{}".format(traceback.format_exc()))
                    for i in range(2):
                        ret_cancel = loop_Image_until(main_maps["find_images"]["叉号"], times=2)
                        if ret_cancel:
                            click([ret_cancel.x, ret_cancel.y, ret_cancel.x + 15, ret_cancel.y + 15], text="点击叉号")
            # 叉掉
            click(cls.cancel, text="点击 叉号")

    @classmethod
    def switch_celestial(cls, select=False):
        """天书 一键 启停"""
        click(mainUI["main_pet_loc"], text="点击 宠物位置")
        click(cls.right_loc["阳间"], text="点击 阳间")
        click(cls.right_loc["技能"], text="点击 技能")
        click(cls.skill["row"]["天书"], text="点击 天书")
        ret = loop_until(cls.skill["find_color"], "一键开启", times=2)
        # if ret:  # 处于开启状态
        if bool(ret) ^ select:
            click(cls.skill["天书"]["一键开启"], text="点击 一键开启")
        # 叉掉
        click(cls.cancel, text="点击 叉号")


# 人物补充
class Man:
    cancel = main_man["叉号"]
    right_col = main_man["right_column"]
    hp = main_man["属性"]["气血"]
    mana = main_man["属性"]["法力"]
    loyal = main_man["属性"]["忠诚"]
    level = main_man["属性"]["等级"]
    level2 = main_man["属性"]["等级2"]
    supply = main_man["属性"]["补充储备"]
    skill = main_man["技能"]
    drugstore = main_man["药店"]

    @classmethod
    def show_attr(cls):
        """点击属性 识别"""
        click(mainUI["main_man_loc"], text="点击 人物")
        click(cls.right_col["属性"], text="点击 属性")
        hp = ocr(cls.hp)
        mana = ocr(cls.mana)
        loyal = ocr(cls.loyal)
        # level = ocr(cls.level)
        level2 = ocr(cls.level2)
        print(level2)
        level = level2[:-1][2:]
        # 叉掉
        click(cls.drugstore["叉号"], text="点击 叉号")
        xp.console("气血:{}; 法力:{}; 忠诚:{}".format(hp, mana, loyal))
        xp.console("等级2:{}".format(level))
        log.info("气血:{}; 法力:{}; 忠诚:{}".format(hp, mana, loyal))
        log.info("等级:{}".format(level))
        try:
            if int(hp) < 10000000:
                xp.console("补充气血")
                log.info("补充气血")
                cls.add_hp()
            if int(mana) < 10000000:
                xp.console("补充法力")
                log.info("补充法力")
                cls.add_mana()
            if int(loyal) < 1000:
                xp.console("补充忠诚")
                log.info("补充忠诚")
                cls.add_loyal()
        except Exception as error:
            print("气血 法力 忠诚 识别有误")
            log.error("气血 法力 忠诚 识别有误;{}".format(traceback.format_exc()))
            click(cls.cancel, text="点击 叉号")
        time.sleep(5)

    @classmethod
    def skill_setting(cls):
        """点击 技能"""
        click(mainUI["main_man_loc"], text="点击 人物")
        click(cls.right_col["技能"], text="点击 技能")
        # xp.console(cls.skill["row"])

        for row in list(cls.skill["row"].keys()):
            click(cls.skill["row"][row], text="点击 {}".format(row))
            for col in list(cls.skill["column"].keys()):
                click(cls.skill["column"][col], text="点击 {}".format(col))
                ret = ocr(cls.skill["column"][col])
                if ret:
                    xp.console("{}->{}->{}".format(row, col, ret))
                    log.info("{}->{}->{}".format(row, col, ret))
                else:
                    break

    @classmethod
    def add_hp(cls, nums=1, level="高级"):
        """点击属性 点击补充储备"""
        click(mainUI["main_man_loc"], text="点击 人物")
        click(cls.right_col["属性"], text="点击 属性")
        click(cls.supply[0], text="点击 补充储备")
        click(cls.supply[1], text="点击 前往药店")
        # 循环找 对话叉号
        loop_until(main_task["find_color"], "人物对话叉号", times=20)
        click(main_task["对话"]["first_button"], text="点击 买卖")
        # 购买血池
        if level != "低级":
            time.sleep(3)
            click(cls.drugstore["{}血池".format(level)], text="点击 {}血池".format(level))
        for i in range(nums):
            time.sleep(3)
            click(cls.drugstore["购买"], text="点击 购买")
            time.sleep(3)
            click(cls.drugstore["使用"], text="点击 使用")
        # 叉掉
        click(cls.drugstore["叉号"], text="点击 叉号")

    @classmethod
    def add_mana(cls, nums=1, level="高级"):
        """点击属性 点击补充储备"""
        click(mainUI["main_man_loc"], text="点击 人物")
        click(cls.right_col["属性"], text="点击 属性")
        click(cls.supply[0], text="点击 补充储备")
        click(cls.supply[1], text="点击 前往药店")
        # 循环找 对话叉号
        loop_until(main_task["find_color"], "人物对话叉号", times=20)
        click(main_task["对话"]["first_button"], text="点击 买卖")
        # 购买灵池
        if level != "低级":
            time.sleep(3)
            click(cls.drugstore["{}灵池".format(level)], text="点击 {}灵池".format(level))
        for i in range(nums):
            time.sleep(3)
            click(cls.drugstore["购买"], text="点击 购买")
            time.sleep(3)
            click(cls.drugstore["使用"], text="点击 使用")
        # 叉掉
        click(cls.drugstore["叉号"], text="点击 叉号")

    @classmethod
    def add_loyal(cls, nums=3):
        """点击属性 点击补充储备"""
        click(mainUI["main_man_loc"], text="点击 人物")
        click(cls.right_col["属性"], text="点击 属性")
        click(cls.supply[0], text="点击 补充储备")
        click(cls.supply[2], text="点击 前往杂货店")
        # 循环找 对话叉号
        loop_until(main_task["find_color"], "人物对话叉号", times=20)
        click(main_task["对话"]["first_button"], text="点击 我要做买卖")
        for i in range(nums):
            time.sleep(3)
            click(cls.drugstore["购买"], text="点击 购买")
            time.sleep(3)
            click(cls.drugstore["使用"], text="点击 使用")
            # 叉掉
        click(cls.drugstore["叉号"], text="点击 叉号")

if __name__ == '__main__':
    Man.show_attr()

# Author: Lovy
# File : 邮件奖励
# Time : 2023-02-26 16:27
from common.generalFunction import *


# 好友 邮件领取
def get_emial_award():
    """选判断节日礼包"""
    # 主界面 循环找图 好友  [并没有考虑 仙风散 宠分散 急急如令令 紫气鸿蒙 不足等]
    res = loop_Image_until(mainUI["find_image"]["好友"], times=10)
    if res:
        click(mainUI["main_friends_loc"], text="点击 好友")
        click(main_friend["邮箱"], text="点击 邮箱")
        click(main_friend["一键领取"], text="点击 一键领取")
        # 使用  确认
        for i in range(6):
            ret = loop_Image_until(main_friend["find_image"]["使用"], times=3)
            if ret:
                click(main_friend["使用"], text="点击 使用")
                # todo 找到使用多个的位置 没有考虑 全局卷轴 使用位置
                images_name = ["使用多个", "使用1个", "使用1ge"]
                for i in images_name:
                    ret1 = loop_Image_until(main_friend["find_image"][i], times=2)
                    if ret1:
                        click([ret1.x, ret1.y, ret1.x + 93, ret1.y + 35], text="点击 %s" % i)

        ret = loop_words_until("节日", *main_friend["识别区域"], times=2)
        if ret:
            click(ret, text="点击 节日礼包")
            click(main_friend["寻路前往"], text="点击 寻路前往")
            loop_until(mainUI["find_colors"], "人物对话叉号", times=20)
            click(mainUI["对话"]["first_button"], text="点击 领取礼包")
        else:
            click(main_friend["收起"], text="点击 收起")

    time.sleep(5)

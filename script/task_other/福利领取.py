# Author: Lovy
# File : 福利领取
# Time : 2023-02-26 16:31
from common.generalFunction import *


# 福利领取
def get_welfare():
    # 循环找图 福利
    res = loop_Image_until(mainUI["find_image"]["福利"], times=3)
    if res:
        # 签到
        click(mainUI["main_welfare_loc"], text="点击 福利")
        xp.swipe(280, 280, 289, 450)
        time.sleep(5)
        # click(main_welfare["签到"]["loc"])
        # 每日签到
        ret = loop_words_until("每", *main_welfare["pos_find"], times=3)
        if ret:
            click(ret, text="点击 每日签到")
            supply_times = ocr(main_welfare["签到"]["补签"])
            xp.console(supply_times)
            start_x, start_y = main_welfare["签到"]["start_loc"]
            w, h = main_welfare["签到"]["rect"]
            status = 0
            for i in range(5):
                if status:
                    break
                for j in range(5):
                    loc_x, loc_y = start_x + j * w, start_y + i * h
                    rec1 = xp.matchColor(main_welfare["签到"]["color"], loc_x, loc_y)
                    rec2 = xp.matchColor(main_welfare["签到"]["color"], loc_x, loc_y + 32)
                    rec = rec1 or rec2
                    xp.console("{}, {} 结果：{}".format(loc_x, loc_y, rec))
                    log.info("{}, {} 结果：{}".format(loc_x, loc_y, rec))
                    if not rec:
                        click([loc_x - 20, loc_y - 20, loc_x + 20, loc_y + 20], text="签到")
                        # 取消 补签
                        res1 = loop_Image_until(main_welfare["find_image"]["补签取消"], times=3)
                        if res1:
                            click(main_welfare["签到"]["补签取消"], text="点击 取消")
                        status = 1
                        break
        # 月道增福
        ret = loop_words_until("月道", *main_welfare["pos_find"], times=3)
        if ret:
            click(ret, text="月道增福")
            for i in range(3):
                ret1 = loop_Image_until(main_welfare["find_image"]["月道领取"], times=3, breakTime=1)
                if ret1:
                    click(main_welfare["月道增福"]["领取"], text="领取")
        # 上划
        xp.swipe(*main_welfare["升级福利"]["slide"])
        time.sleep(2)
        # 神秘大礼
        ret = loop_words_until("大", *main_welfare["pos_find"], times=3)
        if ret:
            click(ret, text="神秘大礼")
            click(main_welfare["神秘大礼"]["全部砸开"], text="全部砸开")
        # 升级福利
        ret = loop_words_until("升级", *main_welfare["pos_find"], times=3)
        if ret:
            click(ret, text="升级福利")
            # 循环图 领取 多循环几次
            for i in range(2):
                ret1 = loop_Image_until(main_welfare["find_image"]["升级领取"], times=5, breakTime=1)
                if ret1:
                    click(main_welfare["升级福利"]["领取"], text="领取")
                    click(main_welfare["升级福利"]["确认"], text="确认")
                    ret2 = loop_Image_until(main_welfare["find_image"]["升级确认"], times=3, breakTime=1)
                    if ret2:
                        click(main_welfare["升级福利"]["确认"], text="确认")
        time.sleep(5)
        # 退出
        click(main_welfare["cancel"], text="点击 叉号")
        time.sleep(5)

# Author: Lovy
# File : 荣耀膜拜
# Time : 2023-02-26 16:29
from common.generalFunction import *


# 荣耀 点击 膜拜
def get_honour_award():
    # 主界面 循环找图 荣耀
    res = loop_Image_until(mainUI["find_image"]["荣耀"], times=5)
    if res:
        click([res.x, res.y, res.x + 40, res.y + 12], text="点击 荣耀")
        res1 = loop_Image_until(mainUI["find_image"]["名人堂"], times=5)
        if res1:
            click([res1.x + 15, res1.y, res1.x + 40, res1.y + 12], text="点击 名人堂")
            res2 = loop_Image_until(main_honour["find_image"]["名人堂叉号"], times=5)
            if res2:
                for i in main_honour["click_locs"]:
                    click(i, text="点赞")
                    time.sleep(1)
            click(main_honour["cancel"], text="叉掉")
    time.sleep(5)

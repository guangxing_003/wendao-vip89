# Author: Lovy
# File : 会员领元宝
# Time : 2023-02-26 16:31
from common.generalFunction import *


# 商城 领取 银元宝
def get_sliver_ingot():
    # 主界面 循环找图 商城
    res = loop_Image_until(mainUI["find_image"]["商城"], times=3)
    if res:
        #
        click(mainUI["main_shoppingMall_loc"], text="点击 商城")
        # 点击会员 先判定
        click(main_shoppingMall["会员"], text="点击 会员")
        # 领取
        click(main_shoppingMall["领取"], text="点击 领取")
        # 退出
        click(main_shoppingMall["cancel"], text="点击 叉号")
    time.sleep(5)
# Author: Lovy
# File : 活跃奖励
# Time : 2023-02-26 16:27
from common.generalFunction import *


# 获得活动 活跃奖励
def get_active_award():
    # TODO 找图：已领取的 和 未领取的没什么区别
    # 思路：判断颜色是否为 #B5A69C 如果不为, 点击领取
    click(mainUI["main_task_loc"], text="点击 活动")
    #
    coordinates = main_task["活跃坐标"]
    count = 0
    for x, y in coordinates:
        if not xp.matchColor("#B5A69C", x, y):
            count += 1
    xp.console(count)
    if count == 1:  # 只能使用一次
        x, y = main_task["活跃坐标"][count - 1]
        click([x - 25, y - 85, x + 25, y - 45], text="点击 %s,%s" % (x, y))
        click(main_task["活跃使用"], text="点击 使用")
    elif count > 1:
        x, y = main_task["活跃坐标"][count - 1]
        click([x - 25, y - 85, x + 25, y - 45], text="点击 %s,%s" % (x, y))
        click(main_task["活跃使用"], text="点击 使用")
        click(main_task["活跃使用"], text="点击 使用")
    click(main_task["cancel"], text="点击 叉号")
    time.sleep(5)

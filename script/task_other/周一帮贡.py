# Author: Lovy
# File : 周一帮贡
# Time : 2023-02-26 16:33
from common.generalFunction import *


# 周一帮派俸禄领取
def get_bangpai_welfare():
    # 每周一执行一次
    if time.strftime("%a") == "Mon":
        # 点击展开
        res = loop_Image_until(mainUI["find_image"]["展开"], times=5)
        if res:
            click([res.x, res.y, res.x + 40, res.y + 2], text="点击 展开")
            res1 = loop_Image_until(mainUI["find_image"]["帮派"], times=5)
            if res1:
                click([res1.x, res1.y, res1.x + 40, res1.y + 2], text="点击 帮派")
                click([1114, 338, 1158, 408], text="点击 福利")
                res2 = loop_Image_until(mainUI["find_image"]["帮派领取"], times=5)
                if res2:
                    click([res2.x + 25, res2.y + 12, res2.x + 85, res2.y + 40], text="点击 帮派领取")
                    loop_until(mainUI["find_colors"], "人物对话叉号", times=10)
                    click(mainUI["对话"]["first_button"], text="点击 领取")
                    loop_until(mainUI["find_colors"], "人物对话叉号", times=10)
                    click(mainUI["对话"]["first_button"], text="点击 离开")
                else:
                    click([1065, 24, 1100, 61], text="点击 退出")
    time.sleep(3)


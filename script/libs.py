# Author: Lovy
# File : libs
# Time : 2023-02-26 16:46
from common.generalFunction import *
from task_branch.修法任务 import goto_xiufa
from task_branch.引魂入殿 import goto_yinhunrudian
from task_branch.竞技场 import goto_jingjichang
from task_branch.通天塔 import goto_tongtian
from task_branch.助人为乐 import goto_zhuren
from task_branch.师门任务 import goto_shimen
from task_branch.帮派任务 import goto_bangpai
from task_branch.帮派日常任务 import goto_bangpairichang, BangPairichang
from task_branch.魔龙之祸 import goto_molong
from task_main.修行 import goto_xiuxing
from task_main.副本 import goto_fuben
from task_main.除暴 import goto_chubao
from task_other.洛书 import get_luoshu_award
from task_other.周一帮贡 import get_bangpai_welfare
from task_other.活跃奖励 import get_active_award
from task_other.福利领取 import get_welfare
from task_other.荣耀膜拜 import get_honour_award
from task_other.会员领元宝 import get_sliver_ingot
from task_other.角色属性 import *
from task_other.邮件奖励 import get_emial_award


# 检测 任务 是否完成
def not_finished_task_ocr():
    abandon_task_names = ["帮派任务", "助人为乐", "随机师门"]
    # 检测任务栏
    switch_zhudui_renwu(task=True)
    # 点击任务
    click(mainUI["main_task_right_loc"]["loc"], text="点击任务")
    # 循环找字
    for task_name in abandon_task_names:
        res = loop_words_until(task_name, *right_task["ocr_loc"], times=2)
        if res:
            click(res, text="点击 {}".format(task_name))
            click(right_task["放弃"], text="点击 放弃")
            click(right_task["确认"], text="点击 确认")

    res = xp.findText("帮派日常", 171, 67, 470, 676)
    print(res)
    res = loop_words_until("帮派日常挑战", *right_task["ocr_loc"], times=2)
    if res:
        click(res, text="点击 {}".format("帮派日常"))
        click(right_task["前往"], text="点击 前往")
        BangPairichang.exec_script()
    else:
        click(mainUI["main_task_right_loc"]["叉号"], text="点击 叉号")


if __name__ == '__main__':
    not_finished_task_ocr()

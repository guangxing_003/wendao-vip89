# Author: Lovy
# File : 除暴
# Time : 2023-02-26 15:40
from common.generalFunction import *


# 除暴
def goto_chubao(double_turn_on=True):
    # 循环找图 李总兵
    res = loop_until(main_task["find_color"], "人物对话叉号")
    if res:
        person_name = ocr(main_task["对话"]["name_loc"])
        xp.console("李总兵检测:", person_name)
        log.info("李总兵检测:{}".format(person_name))
        if person_name:
            click(main_task["对话"]["first_button"], text="点击 任务对话")
            # 判断是否开启双倍
            ret_cancel = loop_Image_until(main_maps["find_images"]["叉号"], times=5)
            if ret_cancel and not double_turn_on:
                # 点击取消
                click(main_task["对话"]["cancel"], text="双倍弹窗: 取消")
            elif ret_cancel and double_turn_on:
                # 点击确认
                click(main_task["对话"]["confirm"], text="双倍弹窗: 确认")
                # 巡逻栏页面 点击 双倍 点击取消
                main_patrol_setting(double=True, jump=True, defence=False)
            else:
                print("没有找到双倍弹窗。。。")
                log.info("没有找到双倍弹窗。。。")

            # 检测右边框是否处于组队还是任务
            switch_zhudui_renwu(task=True)
            # 点击
            rest = ocr(mainUI["main_task_right_loc"]["task_list"][0])
            xp.console(rest)
            rest = ocr(mainUI["main_task_right_loc"]["task_list"][1])
            xp.console(rest)
            rest = ocr(mainUI["main_task_right_loc"]["task_list"][2])
            xp.console(rest)
            click(mainUI["main_task_right_loc"]["task_list"][0])
            xp.console("点击任务列表第一个任务")
            log.info("点击任务列表第一个任务")

            # TODO 战斗设置
            ret_auto_word = loop_Image_until(battle_setting["find_image"]["自动"], times=2,  breakTime=10)
            if ret_auto_word:
                print("点击 自动战斗")
                log.info("点击 自动战斗")
                battle_use(man_defence=True, pet_skill_name="法攻二阶")
            else:
                print("没有 找到 自动战斗")
                log.info("没有 找到 自动战斗")

            # 什么时候除暴结束？ 循环找色 地图 如果 出现天墉城 即任务结束
            xp.console("等待500秒，检测下一轮任务")
            log.info("等待500秒，检测下一轮任务")
            time.sleep(500)
            times = 100
            while times > 0:
                res = loop_until(mainUI["find_colors"], "地图", times=2)
                if res:
                    click(mainUI["maps_location"], text="点击 当前地图名称")
                    res1 = loop_Image_until(main_maps["find_images"]["天墉城"], times=2)
                    if res1:
                        print("当前处于天墉城中心，除暴任务已完成")
                        log.info("当前处于天墉城中心，除暴任务已完成")
                        res2 = loop_Image_until(main_maps["find_images"]["叉号"], times=2)
                        if res2:
                            click([res2.x, res2.y, res2.x + 20, res2.y + 20], text="点击 叉号")
                        break
                    else:
                        res2 = loop_Image_until(main_maps["find_images"]["叉号"], times=2)
                        if res2:
                            click([res2.x, res2.y, res2.x + 20, res2.y + 20], text="点击 叉号")
                times -= 1
                time.sleep(10)

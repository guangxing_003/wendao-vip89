# Author: Lovy
# File : 副本
# Time : 2023-02-26 15:38
from common.generalFunction import *


# 副本循环
def loop_fuben_task(text, skip1, battle, skip2):
    # 死亡 点击继续没有实现
    ret = loop_until(task_fuben["find_color"], "人物对话叉号", times=10)
    if ret:
        ret_name = ocr(task_fuben["对话"]["name_loc"])
        xp.console("人物名称:{}".format(ret_name))
        log.info("人物名称:{}".format(ret_name))
        click(task_fuben["对话"]["first_button"], text=text)
        if skip1:
            loop_until(main_task["find_color"], "跳过", times=10)
            click(main_task["跳过"], text="点击 跳过")
        if battle:
            # todo 自动战斗
            click(task_fuben["battle_auto"], text="点击 自动战斗")
        if skip2 > 1:
            loop_until(main_task["find_color"], "跳过", times=200)
            click(main_task["跳过"], text="点击 跳过")
            loop_until(main_task["find_color"], "跳过", times=200)
            click(main_task["跳过"], text="点击 跳过")
            loop_until(main_task["find_color"], "跳过", times=200)
            click(main_task["跳过"], text="点击 跳过")
        elif skip2:
            loop_until(main_task["find_color"], "跳过", times=200)
            click(main_task["跳过"], text="点击 跳过")
        # 点击任务栏
        click(task_fuben["task_loc"], text="点击 任务栏")
        click(task_fuben["task_loc"], text="点击 任务栏")


# 副本
def goto_fuben():
    # 循环找色 赤灵尊神
    res = loop_until(main_task["find_color"], "人物对话叉号", times=20)
    if res:
        click(main_task["对话"]["first_button"], text="点击 进入副本")
        time.sleep(10)
        """等待 全部同意"""
        # 点击 创建
        click(task_fuben["75级"]["创建"], text="创建 75级")
        # 寻找 跳过
        ret1 = loop_until(main_task["find_color"], "跳过", times=3)
        if ret1:
            click(main_task["跳过"], text="点击 跳过")
            # 识别 陈塘关 or 兰若寺
            ret2 = ocr(task_fuben["task_name"])
            click(task_fuben["task_loc"], text="点击 任务栏")
            if check_ocr(ret2, task_name="陈塘关"):
                for text, skip1, battle, skip2 in fuben_texts["陈塘关"]:
                    loop_fuben_task(text, skip1, battle, skip2)
            elif check_ocr(ret2, task_name="兰若寺"):
                loop_until(main_task["find_color"], "跳过", times=10)
                click(main_task["跳过"], text="点击 跳过")
                click(task_fuben["task_loc"], text="点击 任务栏")
                for text, skip1, battle, skip2 in fuben_texts["兰若寺"]:
                    loop_fuben_task(text, skip1, battle, skip2)
            if loop_until(task_fuben["find_color"], "人物对话叉号", times=10):
                ret4 = ocr(task_fuben["对话"]["name_loc"])
                xp.console(ret4)
                click(task_fuben["对话"]["first_button"], text="感悟良多, 黑山老妖")
                click(task_fuben["对话"]["first_button"], text="谢真人，")

            # 领取副本奖励
            # 循环找色 赤灵尊神
            res = loop_until(main_task["find_color"], "人物对话叉号", times=5)
            if res:
                click(main_task["对话"]["first_button"], text="领取副本奖励")
                click(main_task["对话"]["second_button"], text="领取道行奖励")
            time.sleep(4)
        else:
            res2 = loop_Image_until(main_maps["find_images"]["叉号"], times=2)
            if res2:
                click([res2.x, res2.y, res2.x + 15, res2.y + 15], text="点击 叉号")
                print("副本人数不足3人")
                log.info("副本人数不足3人")


# Author: Lovy
# File : 修行
# Time : 2023-02-26 15:43
import logging

from common.generalFunction import *


def end_xiuxing():
    # 什么时候修行结束？
    time.sleep(1000)
    times = 100
    while times > 0:
        res = loop_until(mainUI["find_colors"], "地图", times=2)
        if res:
            click(mainUI["maps_location"], text="点击 当前地图名称")
            res1 = loop_Image_until(main_maps["find_images"]["骷髅山"], times=2)
            if res1:
                res2 = loop_Image_until(main_maps["find_images"]["叉号"], times=2)
                if res2:
                    click([res2.x, res2.y, res2.x + 20, res2.y + 20], text="点击 叉号")
                """点击任务 寻找修行"""
                if switch_zhudui_renwu(task=True):
                    # 点击任务 前往
                    click(mainUI["main_task_right_loc"]["loc"], text="点击任务")
                    # 循环找字修行
                    ret = loop_words_until("修行", 172, 67, 468, 672, times=2)
                    res3 = loop_Image_until(main_maps["find_images"]["叉号"], times=2)
                    if res3:
                        click([res3.x, res3.y, res3.x + 20, res3.y + 20], text="点击 叉号")
                    if ret:
                        time.sleep(30)
                        continue
                    else:
                        print("当前处于骷髅山，修法任务已完成")
                        log.info("当前处于骷髅山，修法任务已完成")
                        break
                else:
                    time.sleep(30)
                    continue
            else:
                res2 = loop_Image_until(main_maps["find_images"]["叉号"], times=3)
                if res2:
                    click([res2.x, res2.y, res2.x + 20, res2.y + 20], text="点击 叉号")
        times -= 1
        time.sleep(30)


# 修行
def goto_xiuxing(double_turn_on=True):
    # 循环找图 柳如尘
    res = loop_until(main_task["find_color"], "人物对话叉号")
    if res:
        person_name = ocr(main_task["对话"]["name_loc"])
        xp.console("柳如尘检测:{}".format(person_name))
        log.info("柳如尘检测:{}".format(person_name))
        if person_name:
            click(main_task["对话"]["first_button"], text="点击 第一个选项")
            # 如果是离开 点击叉号 退出
            res = loop_until(main_task["find_color"], "人物对话叉号", times=5)
            if res:
                xp.console("人数不足3人 选项{}".format(ocr(main_task["对话"]["first_button"])))
                log.info("人数不足3人 选项{}".format(ocr(main_task["对话"]["first_button"])))
                click(main_task["对话"]["叉号"], text="人数不足三人 叉掉")
                return
            # 判断是否开启双倍
            ret_cancel = loop_Image_until(main_maps["find_images"]["叉号"], times=5)
            # double_switch = loop_until(task_main["find_color"], "双倍", times=3)
            if ret_cancel and not double_turn_on:
                # xp.console("双倍弹窗: 取消")
                # 点击取消
                click(main_task["对话"]["cancel"], text="双倍弹窗: 取消")
            elif ret_cancel and double_turn_on:
                # 点击确认
                click(main_task["对话"]["confirm"], text="双倍弹窗:确认")
                # 巡逻栏页面 点击 双倍 点击取消
                main_patrol_setting(double=True, jump=True, defence=False)
            else:
                print("没有找到双倍弹窗。。。")
                log.info("没有找到双倍弹窗。。。")

            # 检测右边框是否处于组队还是任务
            switch_zhudui_renwu(task=True)
            # 点击
            click(mainUI["main_task_right_loc"]["task_list"][0])
            xp.console("点击任务列表第一个任务")
            log.info("点击任务列表第一个任务")
            end_xiuxing()



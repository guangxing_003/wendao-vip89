# Author: Lovy
# File : 引魂入殿
# Time : 2023-02-26 16:07
from common.generalFunction import *


class Yinhunrudian():
    @classmethod
    def check_events(cls):
        events = ["红衣女", "黄衣女", "赌鬼", "嗜茶鬼", "杨镖头", "牛头", "冰火"]
        if loop_Image_until(difu["find_image"]["点此继续"], times=1, breakTime=0.3):
            return "点此继续"
        for i in range(len(events)):
            res = loop_until(difu["find_color"], events[i], times=1, breakTime=0.3)
            if res:
                return events[i]
        return False

    @classmethod
    def get_nums(cls, s):
        if not s:
            return 0
        nums = ""
        if "A" in s:
            return 4
        if "P" in s:
            return 1
        if "O" in s:
            return 0
        if "S" in s:
            return 9

        for i in s:
            if 48 <= ord(i) <= 57:
                nums += i
        xp.console("引魂幡剩余次数:{}".format(nums))
        log.info("引魂幡剩余次数:{}".format(nums))
        if nums:
            return int(nums)
        return 0

    @classmethod
    def _event_handle(cls):
        # 看看是否是跳过
        if loop_until(difu["find_color"], "跳过", times=2, breakTime=1):
            # 哪个事件
            event = cls.check_events()
            click(difu["跳过"], text="跳过")

            if event == "红衣女":
                click(difu["跳过"], text="跳过")
                # todo 检测 是否处于自动战斗
                # click(difu["自动战斗"], text="自动战斗")
                time.sleep(5)
                loop_until(difu["find_color"], "跳过", times=100, breakTime=1)
                click(difu["跳过"], text="跳过")
            elif event == "赌鬼":
                click(difu["跳过"], text="跳过")
                click(difu["对话"]["叉号"], text="任务对话叉号")
                loop_until(difu["find_color"], "跳过", times=100, breakTime=1)
                click(difu["跳过"], text="跳过")
            elif event == "嗜茶鬼":
                click(difu["跳过"], text="跳过")
                click(difu["嗜茶返回"], text="嗜茶返回")
                click(difu["确认"], text="确认")
            elif event == "杨镖头":
                click(difu["跳过"], text="跳过")
                click(difu["贪吃叉号"], text="贪吃叉号")
                click(difu["确认"], text="确认")
            elif event == "牛头":
                click(difu["跳过"], text="跳过")
                click(difu["点此继续"], text="点此继续")
            elif event == "黄衣女":
                click(difu["跳过"], text="跳过")
                click(difu["拼图叉号"], text="拼图叉号")
                click(difu["确认"], text="确认")
            elif event == "冰火":
                time.sleep(25)

            elif event == "点此继续":
                click(difu["点此继续"], text="点此继续")

        elif loop_Image_until(difu["find_image"]["点此继续"], times=2, breakTime=1):
            click(difu["点此继续"], text="点此继续")

    @classmethod
    def exec_script(cls):
        time.sleep(5)
        # 摇一摇
        click(difu["引魂幡"])
        time.sleep(5)
        cls._event_handle()
        time.sleep(5)
        click(difu["移行卡"])
        click(difu["确认"], text="确认")
        time.sleep(5)
        cls._event_handle()
        times = 20
        while True:
            # times = ocr(difu["times"])
            # xp.console(times)
            # new_times = cls.get_nums(times)
            # if not new_times:
            #     break
            # 摇一摇

            click(difu["引魂幡"], text="引魂幡摇一摇")
            time.sleep(5)
            cls._event_handle()
            times -= 1
            if times < 0:
                break

        # 点击 万能卡
        click(difu["万能卡"], text="点击 万能卡")
        click(difu["steps"]["6"], text="点击 6")
        time.sleep(6)
        cls._event_handle()
        # 点击乌龟卡
        click(difu["乌龟卡"], text="使用 乌龟卡")
        click(difu["确认"], text="确认")
        time.sleep(6)
        cls._event_handle()

        # 退出
        click(difu["退出"], text="退出")
        # 确认
        click(difu["确认"], text="确认")
        time.sleep(5)
        # 点击 使用
        click(difu["使用"], text="使用")
        time.sleep(5)



# 引魂入殿
def goto_yinhunrudian():
    # 循环对话
    res = loop_until(main_task["find_color"], "人物对话叉号", times=30)
    if res:
        click(main_task["对话"]["first_button"], text="点击 引魂入殿")
        Yinhunrudian.exec_script()


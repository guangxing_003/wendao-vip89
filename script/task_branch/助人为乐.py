# Author: Lovy
# File : 助人为乐
# Time : 2023-02-26 15:52
from common.generalFunction import *


# 助人为乐
class ZhuRen:
    """循环等待 人物对话框"""

    @classmethod
    def maosandaoshi(cls):
        loop_until(main_task["find_color"], "跳过", times=30)
        click(main_task["跳过"], text="点击 跳过")
        # 点击任务 前往
        click(mainUI["main_task_right_loc"]["loc"], text="点击任务")
        click(right_task["前往"], text="点击 前往")
        #
        loop_until(task_fuben["find_color"], "人物对话叉号", times=20)
        click(mainUI["对话"]["first_button"], text="{}".format(ocr(mainUI["对话"]["first_button"])))
        loop_until(main_task["find_color"], "跳过", times=3)
        click(main_task["跳过"], text="点击 跳过")
        # 自动战斗
        # click(task_fuben["battle_auto"], text="点击 自动战斗")
        # time.sleep(5)
        if loop_Image_until(battle_setting["find_image"]["道具"], times=10):
            battle_use(man_defence=True, pet_skill_name="法攻二阶")
        loop_until(main_task["find_color"], "跳过", times=100)
        click(main_task["跳过"], text="点击 跳过")
        click(mainUI["main_task_right_loc"]["loc"], text="点击任务")
        click(right_task["前往"], text="点击 前往")
        #
        loop_until(task_fuben["find_color"], "人物对话叉号", times=20)
        click(mainUI["对话"]["first_button"], text="{}".format(ocr(mainUI["对话"]["first_button"])))
        loop_until(main_task["find_color"], "跳过", times=3)
        click(main_task["跳过"], text="点击 跳过")
        time.sleep(5)

        # 循环找任务栏
        loop_until(mainUI["find_colors"], "任务栏", times=100)
        # 点击任务 前往
        click(mainUI["main_task_right_loc"]["loc"], text="点击任务")
        click(right_task["前往"], text="点击 前往")
        loop_until(task_fuben["find_color"], "人物对话叉号", times=20)

    @classmethod
    def qigai(cls):
        #
        loop_until(task_fuben["find_color"], "人物对话叉号", times=20)
        click(mainUI["对话"]["first_button"], text="点击 帮助乞丐")
        click(mainUI["对话"]["second_button"], text="点击 敲诈")
        # 自动战斗
        # time.sleep(5)
        if loop_Image_until(battle_setting["find_image"]["道具"], times=10):
            battle_use(man_defence=True, pet_skill_name="法攻二阶")
        loop_until(main_task["find_color"], "跳过", times=100)
        click(main_task["跳过"], text="点击 跳过")
        # 点击任务 前往
        click(mainUI["main_task_right_loc"]["loc"], text="点击任务")
        click(right_task["前往"], text="点击 前往")
        #
        loop_until(task_fuben["find_color"], "人物对话叉号", times=10)
        click(mainUI["对话"]["first_button"], text="点击 帮助乞丐")
        click(mainUI["对话"]["second_button"], text="点击 敲诈")
        # 自动战斗
        time.sleep(5)
        loop_until(main_task["find_color"], "跳过", times=100)
        click(main_task["跳过"], text="点击 跳过")
        # 点击任务 前往
        click(mainUI["main_task_right_loc"]["loc"], text="点击任务")
        click(right_task["前往"], text="点击 前往")
        #
        loop_until(task_fuben["find_color"], "人物对话叉号", times=10)
        click(mainUI["对话"]["first_button"], text="点击 帮助乞丐")
        click(mainUI["对话"]["second_button"], text="点击 敲诈")
        # 自动战斗
        time.sleep(5)
        loop_until(main_task["find_color"], "跳过", times=100)
        click(main_task["跳过"], text="点击 跳过")
        # 点击任务 前往
        click(mainUI["main_task_right_loc"]["loc"], text="点击任务")
        click(right_task["前往"], text="点击 前往")
        loop_until(task_fuben["find_color"], "人物对话叉号", times=20)

    @classmethod
    def lizongbing(cls):
        loop_until(main_task["find_color"], "跳过", times=30)
        click(main_task["跳过"], text="点击 跳过")
        # 点击任务 前往
        click(mainUI["main_task_right_loc"]["loc"], text="点击任务")
        click(right_task["前往"], text="点击 前往")
        # 点击跳过
        loop_until(main_task["find_color"], "跳过", times=30)
        click(main_task["跳过"], text="点击 跳过")
        # 自动战斗
        # time.sleep(5)
        if loop_Image_until(battle_setting["find_image"]["道具"], times=10):
            battle_use(man_defence=True, pet_skill_name="法攻二阶")
        loop_until(main_task["find_color"], "跳过", times=100)
        click(main_task["跳过"], text="点击 跳过")

        # 点击任务 前往
        click(mainUI["main_task_right_loc"]["loc"], text="点击任务")
        click(right_task["前往"], text="点击 前往")
        loop_until(main_task["find_color"], "跳过", times=30)
        click(main_task["跳过"], text="点击 跳过")
        # 自动战斗
        time.sleep(5)
        loop_until(main_task["find_color"], "跳过", times=100)
        click(main_task["跳过"], text="点击 跳过")

        # 点击任务 前往
        click(mainUI["main_task_right_loc"]["loc"], text="点击任务")
        click(right_task["前往"], text="点击 前往")
        loop_until(task_fuben["find_color"], "人物对话叉号", times=20)

    @classmethod
    def fengxilai(cls):
        loop_until(task_fuben["find_color"], "人物对话叉号", times=20)
        click(mainUI["对话"]["first_button"], text="点击 帮助冯喜来")
        loop_until(main_task["find_color"], "跳过", times=3)
        click(main_task["跳过"], text="点击 跳过")
        # 自动战斗
        # time.sleep(5)
        if loop_Image_until(battle_setting["find_image"]["道具"], times=10):
            battle_use(man_defence=True, pet_skill_name="法攻二阶")
        loop_until(main_task["find_color"], "跳过", times=100)
        click(main_task["跳过"], text="点击 跳过")

        # 点击任务 前往
        click(mainUI["main_task_right_loc"]["loc"], text="点击任务")
        click(right_task["前往"], text="点击 前往")
        loop_until(task_fuben["find_color"], "人物对话叉号", times=20)
        click(mainUI["对话"]["first_button"], text="点击 比试过招")
        loop_until(main_task["find_color"], "跳过", times=3)
        click(main_task["跳过"], text="点击 跳过")
        # 自动战斗
        time.sleep(5)
        loop_until(main_task["find_color"], "跳过", times=100)
        click(main_task["跳过"], text="点击 跳过")

        # 点击任务 前往
        click(mainUI["main_task_right_loc"]["loc"], text="点击任务")
        click(right_task["前往"], text="点击 前往")
        loop_until(main_task["find_color"], "跳过", times=20)
        click(main_task["跳过"], text="点击 跳过")

        # 点击任务 前往
        click(mainUI["main_task_right_loc"]["loc"], text="点击任务")
        click(right_task["前往"], text="点击 前往")
        loop_until(task_fuben["find_color"], "人物对话叉号", times=20)

    @classmethod
    def yangbiaotou(cls):
        loop_until(main_task["find_color"], "跳过", times=30)
        click(main_task["跳过"], text="点击 跳过")
        # 点击任务 前往
        click(mainUI["main_task_right_loc"]["loc"], text="点击任务")
        click(right_task["前往"], text="点击 前往")
        loop_until(main_task["find_color"], "跳过", times=20)
        click(main_task["跳过"], text="点击 跳过")

        click(mainUI["main_task_right_loc"]["loc"], text="点击任务")
        click(right_task["前往"], text="点击 前往")
        loop_until(main_task["find_color"], "跳过", times=20)
        click(main_task["跳过"], text="点击 跳过")

        click(mainUI["main_task_right_loc"]["loc"], text="点击任务")
        click(right_task["前往"], text="点击 前往")
        loop_until(main_task["find_color"], "跳过", times=20)
        click(main_task["跳过"], text="点击 跳过")

        click(mainUI["main_task_right_loc"]["loc"], text="点击任务")
        click(right_task["前往"], text="点击 前往")
        loop_until(main_task["find_color"], "跳过", times=20)
        click(main_task["跳过"], text="点击 跳过")
        # 自动战斗 没有跳过选项 如何等待战斗结束？ 等待任务栏出现
        # time.sleep(5)
        if loop_Image_until(battle_setting["find_image"]["道具"], times=10):
            battle_use(man_defence=True, pet_skill_name="法攻二阶")
        loop_until(mainUI["find_colors"], "任务栏", times=100)
        click(mainUI["main_task_right_loc"]["loc"], text="点击任务")
        click(right_task["前往"], text="点击 前往")
        loop_until(main_task["find_color"], "跳过", times=20)
        click(main_task["跳过"], text="点击 跳过")

        click(mainUI["main_task_right_loc"]["loc"], text="点击任务")
        click(right_task["前往"], text="点击 前往")
        loop_until(main_task["find_color"], "跳过", times=20)
        click(main_task["跳过"], text="点击 跳过")

        click(mainUI["main_task_right_loc"]["loc"], text="点击任务")
        click(right_task["前往"], text="点击 前往")
        loop_until(main_task["find_color"], "跳过", times=20)
        click(main_task["跳过"], text="点击 跳过")

        click(mainUI["main_task_right_loc"]["loc"], text="点击任务")
        click(right_task["前往"], text="点击 前往")
        loop_until(main_task["find_color"], "跳过", times=20)
        click(main_task["跳过"], text="点击 跳过")

        # 点击任务 前往
        click(mainUI["main_task_right_loc"]["loc"], text="点击任务")
        click(right_task["前往"], text="点击 前往")
        loop_until(task_fuben["find_color"], "人物对话叉号", times=20)

    @classmethod
    def ocr_right_task(cls, task_name="助人", help_name="杨镖头"):
        # todo first_button
        help_name = ocr(main_task["对话"]["second_button"])
        xp.console("助人为乐:%s" % help_name)
        log.info("助人为乐:{}".format(help_name))
        click(main_task["对话"]["second_button"], text="点击 帮助")
        switch_zhudui_renwu(task=True)
        click(mainUI["main_task_right_loc"]["loc"], text="点击任务")
        ret = loop_words_until(task_name)
        if ret:
            click(ret, text="点击 助人为乐")
            click(right_task["前往"], text="点击 前往")
            if check_ocr(help_name, task_name="杨镖头"):
                cls.yangbiaotou()
            elif check_ocr(help_name, task_name="茅山道士"):
                cls.maosandaoshi()
            elif check_ocr(help_name, task_name="乞丐"):
                cls.qigai()
            elif check_ocr(help_name, task_name="李总兵"):
                cls.lizongbing()
            elif check_ocr(help_name, task_name="冯喜来"):
                cls.fengxilai()
            else:
                xp.console("助人为乐执行有误")
                log.info("助人为乐执行有误")


# 助人为乐
def goto_zhuren(experience=False, double=False, vip=True):
    # 循环找图 白邦芒
    res = loop_until(main_task["find_color"], "人物对话叉号", times=30)
    if res:
        person_name = ocr(main_task["对话"]["name_loc"])
        xp.console("白邦芒检测:{},{}".format(person_name, vip))
        log.info("白邦芒检测:{},vip:{}".format(person_name, vip))
        if person_name and vip:
            click(main_task["对话"]["first_button"], text="点击 速通")
        else:
            click(main_task["对话"]["first_button"], text="点击 领取")
            '''茅山道士 乞丐 李总兵 杨镖头 冯喜来'''
            # 切换 任务 组队
            ZhuRen.ocr_right_task()
        if experience:
            click(main_task["对话"]["first_button"], text="经验")
        else:
            click(main_task["对话"]["second_button"], text="道行")
        if double:
            click(main_task["对话"]["first_button"], text="双倍")
        else:
            click(main_task["对话"]["second_button"], text="原有奖励")

    time.sleep(5)

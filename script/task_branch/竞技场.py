# Author: Lovy
# File : 竞技场
# Time : 2023-02-26 15:48
from common.generalFunction import *
import traceback


# 竞技场
def goto_jingjichang():
    # 循环找图 竞技使者
    res = loop_until(main_task["find_color"], "人物对话叉号")
    if res:
        person_name = ocr(main_task["对话"]["name_loc"])
        xp.console("竞技使者检测:{}".format(person_name))
        log.info("竞技使者检测:{}".format(person_name))
        click(main_task["对话"]["first_button"], text="点击 竞技场")
        # 转换成整数 可能有误
        try:
            times = int(ocr(main_task["竞技使者"]["remain_times"])[-1])
        except Exception as error:
            log.error("竞技场剩余次数识别有误;{}".format(traceback.format_exc()))
        else:
            xp.console("竞技场剩余次数:{}".format(times))
            log.info("竞技场剩余次数:{}".format(times))
            while times > 0:
                # 点击挑战
                click(main_task["竞技使者"]["battle_boy"], text="点击挑战 童子")
                time.sleep(2)
                click(main_task["竞技使者"]["battle_confirm"], text="点击 确认")
                time.sleep(2)
                # click(main_task["竞技使者"]["battle_auto"], text="点击 自动战斗")
                battle_use(pet_enemy_loc="032", man_defence=True, pet_skill_name="法攻二阶")
                # 循环找色 战斗标志
                res = loop_until(main_task["竞技使者"], "battle_rank")
                if res:
                    times -= 1
                else:
                    xp.console("没有找到竞技场battle标志")
                    log.info("没有找到竞技场battle标志")
            if times == 0 or times == "o":
                xp.console("竞技场执行完成。。。")
                log.info("竞技场执行完成。。。")
        # 循环找色 战斗标志
        res = loop_until(main_task["竞技使者"], "battle_rank")
        if res:
            click(main_task["竞技使者"]["cancel"], text="点击 叉号")
        else:
            xp.console("竞技没有找到 叉号")
            log.info("竞技没有找到 叉号")
    time.sleep(5)

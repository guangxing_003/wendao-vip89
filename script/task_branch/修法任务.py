# Author: Lovy
# File : 修法任务
# Time : 2023-02-26 16:04
from common.generalFunction import *


# 修法任务
def goto_xiufa(vip=True):
    if vip:
        # 循环找色 多宝道人
        res1 = loop_until(main_task["find_color"], "人物对话叉号", times=20)
        if res1:
            person_name = ocr(main_task["对话"]["name_loc"])
            xp.console("多宝道人检测:", person_name)
            log.info("多宝道人检测:{}".format(person_name))
            click(main_task["对话"]["first_button"], text="点击 速通")
            click(main_task["对话"]["second_button"], text="点击 不提交")
            time.sleep(5)
    else:
        res = loop_until(main_task["find_color"], "人物对话叉号", times=20)
        if res:
            click(main_task["对话"]["叉号"], text="点击 叉号")
        print("not vip 暂时做不了")
        log.info("not vip 暂时做不了")
        time.sleep(5)
        return

# Author: Lovy
# File : 帮派任务
# Time : 2023-02-26 15:56
from common.generalFunction import *


# 帮派任务
def goto_bangpai(vip=True):
    if vip:
        # 循环找图 帮派总管
        res = loop_until(main_task["find_color"], "人物对话叉号", times=20)
        if res:
            person_name = ocr(main_task["对话"]["name_loc"])
            xp.console("帮派总管检测:", person_name)
            log.info("帮派总管检测:{}".format(person_name))
            if person_name:
                click(main_task["对话"]["first_button"], text="点击 速通")
            time.sleep(5)
    else:
        # 循环找图 帮派总管
        res = loop_until(main_task["find_color"], "人物对话叉号", times=20)
        if res:
            person_name = ocr(main_task["对话"]["name_loc"])
            xp.console("帮派总管检测:", person_name)
            log.info("帮派总管检测:{}".format(person_name))
            if person_name:
                click(main_task["对话"]["first_button"], text="点击 帮派任务")
                click(main_task["跳过"], text="跳过")
                # 三次 购买
                time.sleep(100)
                for i in range(3):
                    if loop_until(main_task["find_color"], "药店叉号"):
                        click(main_task["帮派购买"], text="购买")
                time.sleep(50)


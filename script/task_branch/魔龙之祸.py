# Author: Lovy
# File : 魔龙之祸
# Time : 2023-02-26 16:03
from common.generalFunction import *


# 魔龙之祸
def goto_molong(vip=True):
    if vip:
        # 循环找色 太白金星
        res = loop_until(main_task["find_color"], "人物对话叉号", times=20)
        if res:
            person_name = ocr(main_task["对话"]["name_loc"])
            xp.console("太白金星检测:", person_name)
            log.info("太白金星检测:{}".format(person_name))
            click(main_task["对话"]["first_button"], text="点击 速通")
            time.sleep(5)
    else:
        res = loop_until(main_task["find_color"], "人物对话叉号", times=20)
        if res:
            click(main_task["对话"]["叉号"], text="点击 叉号")
        print("not vip 暂时做不了")
        log.info("not vip 暂时做不了")
        time.sleep(5)
        return

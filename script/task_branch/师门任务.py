# Author: Lovy
# File : 师门任务
# Time : 2023-02-26 15:47
from common.generalFunction import *


# 师门任务
def goto_shimen(vip=False):
    time.sleep(10)
    # 找任务对话框
    if loop_Image_until(main_task["find_image"]["人物对话叉号"], times=20):
        xp.console(ocr(main_task["对话"]["name_loc"]))
        log.info(ocr(main_task["对话"]["name_loc"]))
        click(main_task["对话"]["first_button"], text="点击 师门任务 一")
        if not vip:
            # 循环找色 跳过 +6
            for i in range(20):
                """循环 逃过 时间过长"""
                if loop_until(main_task["find_color"], "跳过", times=20):
                    click(main_task["跳过"], text="点击 跳过")
                    time.sleep(3)
            time.sleep(100)

    # todo 如果什么都没有 之前的任务没有完成 检测右侧框
    # 循环找图 提交按钮检测 装备提交 和 变身卡提交 位置不一样
    if loop_Image_until(main_task["find_image"]["提交"], times=2):
        click(main_task["提交"], text="点击 装备提交")
        click(main_task["确认"], text="点击 确认")
    elif loop_Image_until(main_task["find_image"]["变身卡提交"], times=2):
        click(main_task["变身卡提交"], text="点击 变身卡提交")
        click(main_task["确认"], text="点击 确认")
    else:
        # 随机师门检测
        res = loop_until(main_task["find_color"], "人物对话叉号", times=30)
        ret2 = ocr(main_task["对话"]["level"])
        xp.console(ret2)
        log.info("师门任务:{}".format(ret2))
        if res:
            click(main_task["对话"]["first_button"], text="点击 随机师门")
            if "特" in ret2:
                click(main_task["跳过"], text="点击 跳过")
                # TODO 不完整，只能是点击第一的选项
                res1 = ocr(main_task["对话"]["answer1"])
                res2 = ocr(main_task["对话"]["answer2"])
                res3 = ocr(main_task["对话"]["answer3"])
                answers = {
                    res1: main_task["对话"]["answer1"],
                    res2: main_task["对话"]["answer2"],
                    res3: main_task["对话"]["answer3"]
                }
                xp.console("{}".format(answers))
                log.info(answers)
                guess_answer = list(answers.keys())[0]
                click(answers[guess_answer], text="点击 %s" % guess_answer)
                # 点击跳过
                click(main_task["跳过"], text="点击 跳过")

            elif "初" in ret2:
                # 点击跳过
                click(main_task["跳过"], text="点击 跳过")
                loop_until(main_task["find_color"], "人物对话叉号", times=20)
                if loop_words_until("神秘来客", *main_task["对话"]["name_loc"], times=5):
                    # xp.console("神秘来客")
                    log.info("神秘来客")
                click(main_task["对话"]["first_button"], text="点击 师门初等")
                # 循环找色跳过
                if loop_until(main_task["find_color"], "跳过", times=30):
                    click(main_task["跳过"], text="点击 跳过")
                if loop_until(main_task["find_color"], "跳过", times=30):
                    click(main_task["跳过"], text="点击 跳过")

            elif "中" in ret2:
                # 点击跳过
                click(main_task["跳过"], text="点击 跳过")
                if loop_until(main_task["find_color"], "跳过", times=20):
                    click(main_task["跳过"], text="点击 跳过")
                if loop_until(main_task["find_color"], "跳过", times=20):
                    click(main_task["跳过"], text="点击 跳过")
                if loop_until(main_task["find_color"], "跳过", times=20):
                    click(main_task["跳过"], text="点击 跳过")
    # 特等 百晓通 没答对
    # 如果是人物对话框 直接叉掉
    if loop_Image_until(main_task["find_image"]["人物对话叉号"], times=5):
        click(main_task["cancel_person"], text="点击 叉号")
    time.sleep(10)

# Author: Lovy
# File : 通天塔
# Time : 2023-02-26 16:01
from common.generalFunction import *


# 通天塔
def goto_tongtian(vip=True):
    if vip:
        # 循环找色 北斗星使
        res1 = loop_until(main_task["find_color"], "人物对话叉号", times=20)
        if res1:
            person_name = ocr(main_task["对话"]["name_loc"])
            xp.console("北斗星使检测:", person_name)
            log.info("北斗星使检测:{}".format(person_name))
            click(main_task["对话"]["first_button"], text="点击 速通")
            time.sleep(2)
            click(main_task["对话"]["second_button"], text="点击 道行")
            res2 = loop_until(main_task["北斗星使"], "color_cancel")
            if res2:
                click([res2.x, res2.y, res2.x + 30, res2.y + 30])
            time.sleep(5)
    else:
        res = loop_until(main_task["find_color"], "人物对话叉号", times=20)
        if res:
            click(main_task["对话"]["叉号"], text="点击 叉号")
        print("not vip 暂时做不了")
        log.info("not vip 暂时做不了")
        time.sleep(5)
        return

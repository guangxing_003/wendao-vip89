- 设置模块搜索路径  把script 设置成 源根
- script/main.py 为队长执行 (请提前组好队伍)
- 本代码仅 适用于 分辨率为"1280x720"
- 默认 vip为False 如果你是会员 请修改第89行`vip=True`
    - 多人任务 【未考虑人数不足问题】
      - 除暴
      - 修行
      - 75级副本(道行 奖励) 【未考虑全员同意】
      
    - 单人任务 
      - 助人为乐
      - 帮派任务
      - 帮派日常任务
      - 竞技场
      - 通天塔(道行 奖励)（非vip暂时做不了）
      - 师门
      - 魔龙之祸（非vip暂时做不了）
      - 修法任务（非vip暂时做不了）
      - 引魂入殿
    
    - 其他任务
      - 活跃奖励
      - 邮件奖励
      - 荣耀 膜拜
      - 洛书 领取 （暂不升级 ）
      - 周一 帮派俸禄
      - 福利 领取 （只能签到 月道福运 神秘大礼 升级福利）
- 新增日志模块 保存在移动端`xiaopy/extra/`
- 新增 wirte_token.py 需要自己填写 百度 申请的token
    - 把`access_token`的值直接粘贴 到`write_token.py` 第8行 `token`里面
    - 执行一次 wirte_token.py 
    - 再执行 main.py





- 百度 申请的token 如下

  - 建议先 [注册百度帐号 (baidu.com)](https://passport.baidu.com/v2/?reg&tpl=tb&u=https://tieba.baidu.com/)
  - 然后**实名认证**
  - 点击【控制台】
    - ![](res/markdown/QQ%E6%88%AA%E5%9B%BE20230307184049.png)
  - 点击【左上角】输入关键词【文字识别】
    - ![](res/markdown/QQ%E6%88%AA%E5%9B%BE20230307184157.png)

  - 点击【去领取】
    - ![](res/markdown/QQ%E6%88%AA%E5%9B%BE20230307184236.png)
  - 点击 【全部】 【创建】
    - ![](res/markdown/QQ%E6%88%AA%E5%9B%BE20230307193614.png)
  - 点击 【公有云0个】
    - ![](res/markdown/QQ%E6%88%AA%E5%9B%BE20230307220220.png)
  - 点击 【创建应用】 名字 描述 随便填 【应用所属】选 个人
    - ![](res/markdown/QQ%E6%88%AA%E5%9B%BE20230307220311.png)
  - 复制 自己的 【APIKey】【SecretKey】
  - 粘贴 点击【API在线调试】 复制`access_token`的值 即可
    - ![](res/markdown/QQ%E6%88%AA%E5%9B%BE20230307222150.png)

  